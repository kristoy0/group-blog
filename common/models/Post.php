<?php

namespace common\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\FileHelper;

/**
 * This is the model class for table "{{%post}}".
 *
 * @property int $id
 * @property string $title
 * @property string|null $content_text
 * @property string|null $content_media
 * @property int $status
 * @property int|null $created_by
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property User $createdBy
 */
class Post extends \yii\db\ActiveRecord
{
    const STATUS_DRAFT = 0;
    const STATUS_PUBLISHED = 1;

    public $content_file;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%post}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            [
                'class' => BlameableBehavior::class,
                'updatedByAttribute' => false
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'status'], 'required'],
            [['content_text'], 'string'],
            [['content_file'], 'image'],
            [['status', 'created_by', 'created_at', 'updated_at'], 'integer'],
            [['title', 'content_media'], 'string', 'max' => 255],
            [['status'], 'default', 'value' => self::STATUS_DRAFT],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['created_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'content_text' => Yii::t('app', 'Content Text'),
            'content_media' => Yii::t('app', 'Content Media'),
            'status' => Yii::t('app', 'Status'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function getStatusLabels()
    {
        return [
            self::STATUS_DRAFT => Yii::t('app', 'Draft'),
            self::STATUS_PUBLISHED => Yii::t('app', 'Published')
        ];
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        if ($this->content_file) {
            $this->content_media = Yii::$app->security->generateRandomString(16) . '.' . $this->content_file->extension;
        }

        $transaction = Yii::$app->db->beginTransaction();
        $saved = parent::save($runValidation, $attributeNames);

        if ($saved && $this->content_file) {
            $file_path = Yii::getAlias('@frontend/web/uploads/');

            if (!FileHelper::createDirectory($file_path) || !$this->content_file->saveAs($file_path . $this->content_media)) {
                $transaction->rollBack();

                return false;
            }
        }

        $transaction->commit();

        return $saved;
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return \yii\db\ActiveQuery|\common\models\query\UserQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\query\PostQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\PostQuery(get_called_class());
    }
}
