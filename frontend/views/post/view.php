<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Post */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Posts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="post-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <p class="text-muted"><?= Yii::$app->formatter->asRelativeTime($model->created_at) ?></p>

    <img width="100%" src="<?= Yii::$app->urlManager->createUrl('uploads/' . $model->content_media) ?>">
    <p><?= $model->content_text ?></p>

</div>