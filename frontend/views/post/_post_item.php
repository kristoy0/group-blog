<?php

use yii\helpers\StringHelper;
use yii\helpers\Url;

/** @var $model \common\models\Post */
?>
<div class="media">
    <a href="<?= Url::to(['/post/view', 'id' => $model->id]) ?>">
        <div class="media-left">
            <img style="max-height: 85px;" class="media-object" src="<?= Yii::$app->urlManager->createUrl('uploads/' . $model->content_media) ?>">
        </div>
        <div class="media-body">
            <h4 class="media-heading"><?= $model->title ?></h4>
            <p class="text-muted"><?= StringHelper::truncateWords($model->content_text, 8) ?></p>
            <p class="text-muted"><?= Yii::$app->formatter->asRelativeTime($model->created_at) ?></p>
        </div>
    </a>
</div>